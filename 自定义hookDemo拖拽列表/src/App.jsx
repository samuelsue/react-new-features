import "./styles.css";
import useDraggable from "./useDraggable";

const list = [
  {
    src:
      "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=615503608,4154018040&fm=26&gp=0.jpg",
    title: "万事屋找我"
  },
  {
    title: "吃吃吃……",
    src:
      "https://gss0.baidu.com/94o3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/5d6034a85edf8db1a3bc62bd0223dd54574e745c.jpg"
  },
  {
    title: "汪汪",
    src:
      "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=968093909,4033932240&fm=26&gp=0.jpg"
  }
];

// 返回样式字符串,
function cls(def, ...conditions) {
  const list = [def];
  conditions.forEach((cond) => {
    if (cond[0]) {
      list.push(cond[1]);
    }
  });
  return list.join(" ");
}

export default function App() {
  return (
    <div className="App">
      <DraggableList list={list} />
    </div>
  );
}

function DraggableList({ list }) {
  const { dragList, createDropProps, createDragProps } = useDraggable(list);
  return dragList.map((item, index) => {
    if (item.type === "BAR") {
      return <Bar key={item.id} id={index} {...createDropProps(index)} />;
    } else {
      return (
        <Draggable key={item.id} id={index} {...createDragProps(index)}>
          <Card {...item.data} />
        </Draggable>
      );
    }
  });
}

//List
// Draggable
// Bar
// Draggable
// Bar

function Draggable({ children, id, dragging, eventHandlers }) {
  return (
    <div
      className={cls("draggable", [id === dragging, "dragging"])}
      draggable={true}
      {...eventHandlers}
    >
      {children}
    </div>
  );
}

function Bar({ id, dragOver, dragging, eventHandlers }) {
  if (id === dragging + 1) {
    return null;
  }
  return (
    <div
      className={cls("draggable--bar", [id === dragOver, "dragover"])}
      {...eventHandlers}
    >
      <div
        className="inner"
        style={{
          height: id === dragOver ? 80 : 0
        }}
      ></div>
    </div>
  );
}

function Card({ src, title }) {
  return (
    <div className="card">
      <img src={src} />
      <span>{title}</span>
    </div>
  );
}
