import React, { useState } from "react";

const DRAGGABLE = "DRAGGABLE";
const BAR = "BAR";

function draggable(item, id) {
  return {
    type: DRAGGABLE,
    id,
    data: item
  };
}

function insertBars(list) {
  let i = 0; // ID

  const newBar = () => {
    return {
      type: BAR,
      id: i++
    };
  };

  // |A|B|C|
  return [newBar()].concat(
    ...list.map((item) => {
      return [draggable(item, i++), newBar()];
    })
  );
}

// 计算交换后的list
function calcChanging(list, drag, drop) {
  // drag到相邻的bar上不需要变换数据
  if (Math.abs(drag - drop) === 1) return list;
  list = list.slice();
  const targetIndex = drag - drop > 0 ? drop + 1 : drop - 1;
  const temp = list[drag];
  list[drag] = list[targetIndex];
  list[targetIndex] = temp;
  return list;
}

export default function useDraggable(list) {
  const [dragList, setDragList] = useState(() => {
    return insertBars(list);
  });
  // dragOver保存的是dragOver
  const [dragOver, setDragOver] = useState(null);
  // dragging 保存的是当前拖动的id
  const [dragging, setDragging] = useState(null);

  return {
    dragList,
    createDropProps: (id) => {
      return {
        dragOver,
        dragging,
        eventHandlers: {
          onDragOver: (e) => {
            e.preventDefault();
            setDragOver(id);
          },
          onDragLeave: (e) => {
            e.preventDefault();
            setDragOver(null);
          },
          onDrop: (event) => {
            event.preventDefault();
            setDragOver(null);
            setDragList((list) => calcChanging(list, dragging, id));
          }
        }
      };
    },
    createDragProps: (id) => {
      return {
        dragging,
        eventHandlers: {
          onDragStart: () => {
            setDragging(id);
          },
          onDragEnd: () => {
            setDragging(null);
          }
        }
      };
    }
  };
}
