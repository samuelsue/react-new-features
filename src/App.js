import React, {Component} from 'react';
import StateDemo from "./components/stateDemo";
import LazyLoadDemo from "./components/lazyLoad";
import {BrowserRouter} from "react-router-dom";
import UseStateDemo from "./components/HooksDemo/stateDemo";
import EffectDemo from "./components/HooksDemo/effectDemo";
import RefDemo from "./components/HooksDemo/refDemo";
import ContextDemo from "./components/ContextDemo/contextDemo";
import PureParent from "./components/PureComponentDemo";
import RenderDemo from "./components/renderProps";
import ErrorParent from "./components/ErrorBoundary/Parent";
import MemoDemo from "./components/HooksDemo/MemoDemo";
import CallbackDemo from "./components/HooksDemo/CallbackDemo";
import ImperativeHandleDemo from "./components/HooksDemo/ImperativeHandleDemo";
import ReducerDemo from "./components/HooksDemo/reducerDemo"
import CustomHook from "./components/HooksDemo/CustomHook";

class App extends Component {
  render() {
    return (
      <div>
        <StateDemo/>
        <hr/>
        <BrowserRouter>
          <LazyLoadDemo/>
        </BrowserRouter>
        <hr/>
        <UseStateDemo/>
        <hr/>
        <EffectDemo/>
        <hr/>
        <RefDemo/>
        <hr/>
        <ContextDemo/>
        <hr/>
        <PureParent/>
        <hr/>
        <RenderDemo/>
        <hr/>
        <ErrorParent/>
        <hr/>
        <MemoDemo/>
        <hr/>
        <CallbackDemo/>
        <hr/>
        <ImperativeHandleDemo/>
        <hr/>
        <ReducerDemo/>
        <hr/>
        <CustomHook/>
      </div>
    );
  }
}

export default App;
