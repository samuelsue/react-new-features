import React, {Component} from 'react';

class StateDemo extends Component {
  state = {
    sum: 0
  }
  addOne = () => {
    /*setState是异步更新的，接收第二个参数是一个函数，作为state更新完成的回调*/
    this.setState((prevState, props) => {
      console.log(prevState, props);
      return {
        sum: ++prevState.sum
      }
    }, () => {
      console.log('更新完成了')
      console.log(this.state);
    })
  }

  render() {
    const {sum} = this.state
    return (
      <div>
        <h1>当前求和为{sum}</h1>
        <button onClick={this.addOne}>+1</button>
      </div>
    );
  }
}

export default StateDemo;
