import React, {useEffect, useState} from 'react';

const EffectDemo = () => {
  const [num, updateNum] = useState(0);

  /* useEffect(在函数式组件中执行副作用操作，也就是模拟生命周期钩子):第一个参数是一个函数, 第二个参数如果需要传递，则为一个数组，里面包含要监听的对象(变量)
  如果不传第二个参数，那么会在每次render()后调用, 类似componentDidUpdate回调的功能
  如果传递第二个参数为[], 则表示要监听的列表为空，类似componentDidMount回调的功能
  如果第二个参数为[a,b,c....], 在render后其中一个变量发生变化，则useEffect会再次运行"
  第一个参数函数的返回值也是一个函数，在组件卸载时会被调用.类似componentWillMount回调的功能
   */
  useEffect(() => {
    const timmer = setInterval(() => {
      updateNum(n => n + 1)
    }, 1000)
    return () => {
      clearInterval(timmer);
    };
  }, [])

  return (
    <div>
      <h2>effect--当前计数:{num}</h2>
      <button onClick={() => updateNum(n => n + 1)}>+1</button>
    </div>
  );
};

export default EffectDemo;
