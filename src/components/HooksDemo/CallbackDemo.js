import React, {useCallback, useState} from 'react';

const CallbackDemo = () => {
  let [num, setNum] = useState(0);
  /*
  useCallback类似useMemo，也是缓存，不过它缓存的是一个函数的状态，
  即整个函数的变量和返回值的状态.
  */
  let callback = useCallback(() => {
    console.log('num更新：', num)  // 这里num永远是不变的
    return num
    // eslint-disable-next-line
  }, [])
  return (
    <div>
      <h2>useCallback</h2>
      <p>num:{num}</p>
      <p>callback:{callback()}</p>
      <button onClick={() => setNum(num + 1)}>+1</button>
    </div>
  );
};

export default CallbackDemo;
