import React, {forwardRef, useRef, useImperativeHandle} from 'react';

const ImperativeHandleDemo = () => {
  const sonRef = useRef();
  return (
    <div>
      <h2>useImperativeHandle: 我是父组件</h2>
      <Son ref={sonRef}/>
      {/*父组件中的ref可以获取子组件暴露的方法和属性*/}
      <button onClick={() => sonRef.current.showInput()}>在父组件执行子组件的方法
      </button>
    </div>
  );
};

const Son = forwardRef((props, ref) => {
  const inputRef = useRef()
  // 第三个参数是监听列表，决定什么时候更新暴露给父组件的方法|属性
  useImperativeHandle(ref, () => ({// 暴露给父组件的方法|属性
    showInput: () => {
      alert(inputRef.current.value)
    }
  }))
  return (
    <div>
      <h2>我是子组件</h2>
      <input type="text" ref={inputRef}/>
    </div>
  )
})

export default ImperativeHandleDemo;
