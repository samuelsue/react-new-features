import React, {useMemo, useState} from 'react';

const MemoDemo = () => {
  let [count, setCount] = useState(0);
  /*
  useMemo使用js缓存技术,用于缓存状态(state,props)，这里主要的作用是模拟shouldComponentUpdate生命周期
  useMemo接收两个参数:
  1. 函数，返回值为新的状态
  2. 数组 || undefined, 若为空数组，表示不监听任何state|props的改变，不会触发函数的执行来更新视图,
      若不传(undefined), 默认监听任何state|props的改变，触发函数执行，来更新视图
  */
  let res = useMemo(() => {
    return count;
  }, [count])
  return (
    <div>
      <h2>useMemo: count={res}</h2>
      <button onClick={() => setCount(count + 1)}>+1</button>
      <Son count={count}/>
    </div>
  );
};

const Son = (props) => {
  const {count} = props
  let res = useMemo(() => {
    return count
    // eslint-disable-next-line
  }, [])
  return (
    <h2>子组件接收父组件，不更新视图:{res}</h2>
  )
}

export default MemoDemo;
