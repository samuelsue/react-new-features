import React, {useState, useEffect, useCallback, memo} from 'react';

function useSize() {
  const [size, setSize] = useState({
    width: document.documentElement.clientWidth,
    height: document.documentElement.clientHeight
  })
  const callback = useCallback(() => {
    setSize({
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight
    })
  },[])
  useEffect(()=>{
    window.addEventListener('resize',callback)
    return ()=>{
      window.removeEventListener('resize',callback)
    }
  },[])
  return size
}

const CustomHook = memo(() => {
  const size = useSize()
  return (
    <div>
      <h3>自定义Hook监听浏览器页面大小:{size.width}x{size.height}</h3>
    </div>
  );
});

export default CustomHook;
