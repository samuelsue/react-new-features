import React from 'react';
import {useRef} from "react";

const RefDemo = () => {
  const inputRef = useRef();

  function show() {
    window.alert(inputRef.current.value);
  }

  return (
    <div>
      <h2>refHook</h2>
      <input type="text" ref={inputRef}/>
      <button onClick={show}>点我alert输入</button>
    </div>
  );
};

export default RefDemo;
