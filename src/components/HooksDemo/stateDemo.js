import {useState} from 'react'

export default function UseStateDemo() {
  //  useState传入初始状态，返回一个数组[状态对象，更新状态对象的函数]
  const [count, setCount] = useState(0);

  function add() {
    setCount(prevState => prevState + 1)
  }

  return (
    <div>
      <h2>useState--当前求和为:{count}</h2>
      <button onClick={add}>+1</button>
    </div>
  )
}
