import React, {createContext, useReducer} from 'react';
import {reducer} from './reducer';
import Son1 from "./son1";
import Son2 from "./son2";

export const MyContext = createContext();
const {Provider} = MyContext;

const Index = (props) => {
  const [state, dispatch] = useReducer(reducer, {
    name: 'Samuel',
    age: 26
  })
  return (
    <Provider value={{state, dispatch}}>
      {props.children}
    </Provider>
  );
};

const App = () => {
  return (
    <Index>
      <h3>useReducer结合useContext模拟Redux状态管理</h3>
      <Son1/>
      <Son2/>
    </Index>
  )
}
export default App;
