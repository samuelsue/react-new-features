import React, {useContext} from 'react';
import {MyContext} from "./index";

const Son2 = () => {
  const {state, dispatch} = useContext(MyContext); // 获取context中的对象
  return (
    <div>
      <p>son2组件--name:{state.name},age:{state.age}</p>
      <button onClick={() => dispatch({
        type: 'setAge'
      })}>setAge
      </button>
    </div>
  );
};

export default Son2;
