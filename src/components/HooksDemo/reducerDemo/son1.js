import React, {useContext} from 'react';
import {MyContext} from "./index";

const Son1 = () => {
  const {state, dispatch} = useContext(MyContext); // 获取context中的对象
  return (
    <div>
      <p>son1组件--name:{state.name},age:{state.age}</p>
      <button onClick={() => dispatch({
        type: 'setName'
      })}>setName
      </button>
    </div>
  );
};

export default Son1;
