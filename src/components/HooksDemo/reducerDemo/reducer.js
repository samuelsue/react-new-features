export const reducer = (state, action) => {
  switch (action.type) {
    case 'setName':
      return {
        ...state,
        name: state.name + 'good'
      }
    case 'setAge':
      return {
        ...state,
        age: state.age + 1
      }
    default:
      return state;
  }
}
