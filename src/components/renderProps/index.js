import React, {Component} from 'react';
import './index.css'

class RenderDemo extends Component {
  render() {
    return (
      <div className="parent">
        <h3>演示renderProps</h3>
        <h3>我是Parent组件</h3>
        {/* 形成父子，如果A有state要传递个B？
        <A>
          <B/>
        </A>*/}
        {/*传递一个props叫render，是一个函数，返回值为B组件.实现类似Vue插槽的技术*/}
        <A render={(name) => <B name={name}/>}/>
      </div>
    );
  }
}

class A extends Component {
  state = {
    name: 'Sam'
  }

  render() {
    return (
      <div className="a">
        <h3>我是A组件</h3>
        {/*this.props.children来获取标签体内容*/}
        {/*{this.props.children}*/}
        {this.props.render(this.state.name)}
      </div>
    )
  }
}

class B extends Component {
  render() {
    return (
      <div className="b">
        <h3>我是B组件</h3>
        <p>接收到的参数: {this.props.name}</p>
      </div>
    )
  }
}

export default RenderDemo;
